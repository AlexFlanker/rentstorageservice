package vvvteam.rentservice.domain;

import lombok.Data;
import org.springframework.data.annotation.Id;

import java.util.Date;

@Data
public class BleInfo {
    @Id
    private String id;
    private Integer motion;
    private Integer vbat;
    private Integer rssi;
    private String mac;
    private Date time;
}
