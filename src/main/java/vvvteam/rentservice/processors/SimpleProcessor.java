package vvvteam.rentservice.processors;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import vvvteam.rentservice.domain.BleInfo;

@Slf4j
public class SimpleProcessor implements Processor {
    private final ObjectMapper objectMapper;
    public SimpleProcessor() {
        objectMapper = new ObjectMapper();
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        String jsonBody = new String((byte[]) exchange.getIn().getBody());
        BleInfo bodyConverted = objectMapper.readValue(jsonBody, BleInfo.class);
        exchange.getIn().setBody(bodyConverted);
    }
}
