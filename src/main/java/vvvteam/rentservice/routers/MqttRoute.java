package vvvteam.rentservice.routers;

import lombok.Getter;
import lombok.Setter;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import vvvteam.rentservice.processors.SimpleProcessor;

@Component
@Getter
@Setter
@ConfigurationProperties("camel.component.mqtt")
public class MqttRoute extends RouteBuilder {
    private String host;
    private String userName;
    private String password;

    @Override
    public void configure() {
        from(new MqttQueryBuilder(host, "test", userName, password, "rent/msg").toString())
                .routeId("rent-main-route")
                .group("job-object-store")
                .tracing()
                .process(new SimpleProcessor())
                .to("log:?showHeaders=true")
                .to("bean:bleInfoRepo?method=save(${body})");
    }
}
