package vvvteam.rentservice.routers;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

public class MqttQueryBuilder {

    private final Map<String, Object> queryParams = new HashMap<>();
    private final String topicSubcribe;
    private final String host;
    private final String username;
    private final String password;
    private final String mqttName;

    public MqttQueryBuilder(
            String host,
            String mqttName,
            String username,
            String password,
            String topicToSubscibe) {
        this.host = host;
        this.username = username;
        this.password = password;
        this.mqttName = mqttName;
        this.topicSubcribe = topicToSubscibe;

        queryParams.put("clientId", UUID.randomUUID().toString());
        // без подтверждения
        queryParams.put("qualityOfService", "AtMostOnce");
        queryParams.put("cleanSession", false);
        queryParams.put("lazySessionCreation", true);
        queryParams.put("keepAlive", 10);
        queryParams.put("host", this.host);
        queryParams.put("userName", this.username);
        queryParams.put("password", this.password);
    }

    @Override
    public String toString() {
        StringBuilder route = new StringBuilder("mqtt:");
        route.append(mqttName).append("?").append("subscribeTopicNames=").append(topicSubcribe);
        String params = queryParams.entrySet().stream()
                .map(entry -> entry.getKey() + "=" + entry.getValue())
                .collect(Collectors.joining("&","&",""));
        return route.append(params).toString();
    }
}
