package vvvteam.rentservice.repo;

import org.springframework.data.mongodb.repository.MongoRepository;
import vvvteam.rentservice.domain.BleInfo;

public interface BleInfoRepo extends MongoRepository<BleInfo, String> {
}
