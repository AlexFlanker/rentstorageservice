# RentStorageService #

Соборщик данных от BLE-маячкой. Дергает сообщения из брокера и сохраняет в БД.

### Необходимая инфраструктура ###

* MQTT-брокер Mosquitto [скачать можно отсюда](https://mosquitto.org/download/)
* MQTT-explorer для отладки [скачать можно отсюда](http://mqtt-explorer.com/)
* Установленная MongoDb 


### Конфигурация ###

Все необходимые конфиги можно установить в application.yml. Название топика можно понять в классе MqttRoute(по умолчанию "rent/msg")
